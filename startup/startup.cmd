#@field POWERMETER_IP
#@type STRING
#IP address of the IME PowerMeter.

require modbus,0+
 
 
#Specify the TCP endpoint and give your bus a name eg. "tcpcon1".
drvAsynIPPortConfigure("tcpcon1","$(POWERMETER_IP):502",0,0,1)
#Configure the interpose for TCP
modbusInterposeConfig("tcpcon1", 0, 40000, 10) 

